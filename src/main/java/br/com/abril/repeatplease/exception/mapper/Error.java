package br.com.abril.repeatplease.exception.mapper;

public class Error {

	private int status;
	private String message;

	public Error(String message) {
		this.message = message;
	}

	public Error() {
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
