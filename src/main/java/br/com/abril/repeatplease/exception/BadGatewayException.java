package br.com.abril.repeatplease.exception;

public class BadGatewayException extends Exception {

	private static final long serialVersionUID = -3584908582773414402L;
	
	public BadGatewayException(String message){
		super(message);
	}

}
