package br.com.abril.repeatplease.exception;

public class InternalServerErrorException extends Exception {

	private static final long serialVersionUID = -2954265076055471238L;

	public InternalServerErrorException(String message){
		super(message);
	}

	public InternalServerErrorException() {
		super("");
	}
}
