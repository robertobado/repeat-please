package br.com.abril.repeatplease.exception;

import org.apache.http.HttpResponse;

public class BadResponseFromGatewayException extends Exception {

	private static final long serialVersionUID = 4852797453711646271L;
	
	HttpResponse httpResponse;

	public HttpResponse getHttpResponse() {
		return httpResponse;
	}

	public void setHttpResponse(HttpResponse httpResponse) {
		this.httpResponse = httpResponse;
	}

}
