package br.com.abril.repeatplease.exception.mapper;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.abril.repeatplease.exception.BadGatewayException;

@Provider
public class BadGatewayExceptionMapper extends BaseExceptionMapper implements
ExceptionMapper<BadGatewayException>{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public Response toResponse(BadGatewayException exception) {
		logger.debug(this.getClass().getName() + " capturou uma exceção: "
				+ exception);

		Error error = new Error(exception.getMessage());
		error.setStatus(502);
		
		ResponseBuilder responseBuilder = Response
				.status(502);
		responseBuilder.type(MediaType.APPLICATION_JSON);
		responseBuilder.entity(gson.toJson(error));

		return responseBuilder.build();
	}

}
