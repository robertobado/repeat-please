package br.com.abril.repeatplease.exception.mapper;

import java.io.IOException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.ExceptionMapper;

import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.abril.repeatplease.exception.BadResponseFromGatewayException;
import br.com.abril.repeatplease.util.StringUtilities;

import com.mysql.jdbc.StringUtils;

public class BadResponseFromGatewayExceptionMapper extends BaseExceptionMapper
		implements ExceptionMapper<BadResponseFromGatewayException> {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public Response toResponse(BadResponseFromGatewayException exception) {
		logger.debug("BadResponseFromGatewayExceptionMapper captured an exception: "
				+ exception);

		HttpResponse httpResponse = exception.getHttpResponse();

		ResponseBuilder responseBuilder = Response.status(httpResponse
				.getStatusLine().getStatusCode());
		String contentType = httpResponse.getFirstHeader("Content-Type")
				.getValue();
		if (!StringUtils.isEmptyOrWhitespaceOnly(contentType)) {
			responseBuilder.type(MediaType.valueOf(contentType));
		}
		
		try {
			String responseBody;
			responseBody = StringUtilities.httpResponseAsString(httpResponse);
			responseBuilder.entity(responseBody);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return responseBuilder.build();
	}

}
