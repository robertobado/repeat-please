package br.com.abril.repeatplease.resource;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriUtils;

import br.com.abril.repeatplease.exception.InternalServerErrorException;
import br.com.abril.repeatplease.filter.container.FilterContainer;
import br.com.abril.repeatplease.filter.spec.request.DeleteRequestFilter;
import br.com.abril.repeatplease.filter.spec.request.GetRequestFilter;
import br.com.abril.repeatplease.filter.spec.request.PostRequestFilter;
import br.com.abril.repeatplease.filter.spec.request.PutRequestFilter;
import br.com.abril.repeatplease.filter.spec.response.DeleteResponseFilter;
import br.com.abril.repeatplease.filter.spec.response.GetResponseFilter;
import br.com.abril.repeatplease.filter.spec.response.PostResponseFilter;
import br.com.abril.repeatplease.filter.spec.response.PutResponseFilter;
import br.com.abril.repeatplease.util.SystemConfiguration;

@Path("/{subResources:.*}")
@Component
public class ProxyResource {
	
	private static final String PATH = SystemConfiguration.getPropertyAsString(SystemConfiguration.BASE_TARGET_URL_KEY);

	@Autowired
	private FilterContainer filterContainer;
	
	@Context
	private HttpServletRequest incommingRequest;
	
	@POST
	@Transactional(readOnly = true)
	public Response post() throws Throwable {
		String requestPath = getRequestPath();
		
		HttpPost request = generateBasicHttpPost(requestPath);
		
		return processRequest(request);
	}
	
	@PUT
	@Transactional(readOnly = true)
	public Response put() throws Throwable {
		String requestPath = getRequestPath();
		
		HttpPut outgoingRequest = generateBasicHttpPut(requestPath);
		
		return processRequest(outgoingRequest);
	}
	
	@GET
	@Transactional(readOnly = true)
	public Response get() throws Throwable {		
		String requestPath = getRequestPath();
		
		HttpGet outgoingRequest = generateBasicHttpGet(requestPath);
		
		return processRequest(outgoingRequest);

	}
	
	@DELETE
	@Transactional(readOnly = true)
	public Response delete() throws Throwable {		
		String requestPath = getRequestPath();
		
		HttpDelete outgoingRequest = generateBasicHttpDelete(requestPath);
		
		return processRequest(outgoingRequest);

	}
	
	private Response processRequest(HttpUriRequest outgoingRequest) throws Throwable {
		
		processRequestFilters(outgoingRequest);
		
		HttpResponse incommingResponse = getHttpClient().execute(outgoingRequest);

		ResponseBuilder outgoingResponse = Response.ok();

		processResponseFilters(outgoingRequest, incommingResponse, outgoingResponse);
		
		return outgoingResponse.build();
	}
	
	private void processRequestFilters(HttpUriRequest outgoingRequest) throws Throwable{
		if (outgoingRequest instanceof HttpGet) {
			for(GetRequestFilter filter : filterContainer.getGetRequestFilters()){
				filter.processGetRequestFilter(incommingRequest, outgoingRequest);
			}
		} else if (outgoingRequest instanceof HttpPost) {
			for(PostRequestFilter filter : filterContainer.getPostRequestFilters()){
				filter.processPostRequestFilter(incommingRequest, outgoingRequest);
			}
		} else if (outgoingRequest instanceof HttpPut) {
			for(PutRequestFilter filter : filterContainer.getPutRequestFilters()){
				filter.processPutRequestFilter(incommingRequest, outgoingRequest);
			}
		} else if (outgoingRequest instanceof HttpDelete) {
			for(DeleteRequestFilter filter : filterContainer.getDeleteRequestFilters()){
				filter.processDeleteRequestFilter(incommingRequest, outgoingRequest);
			}
		}
	}
	
	private void processResponseFilters(HttpUriRequest outgoingRequest, HttpResponse incommingResponse, ResponseBuilder outgoingResponse) throws Throwable{
		if (outgoingRequest instanceof HttpGet) {
			for(GetResponseFilter filter : filterContainer.getGetResponseFilters()){
				filter.processGetResponseFilter(incommingRequest, outgoingRequest, incommingResponse, outgoingResponse);
			}
		} else if (outgoingRequest instanceof HttpPost) {
			for(PostResponseFilter filter : filterContainer.getPostResponseFilters()){
				filter.processPostResponseFilter(incommingRequest, outgoingRequest, incommingResponse, outgoingResponse);
			}
		} else if (outgoingRequest instanceof HttpPut) {
			for(PutResponseFilter filter : filterContainer.getPutResponseFilters()){
				filter.processPutResponseFilter(incommingRequest, outgoingRequest, incommingResponse, outgoingResponse);
			}
		} else if (outgoingRequest instanceof HttpDelete) {
			for(DeleteResponseFilter filter : filterContainer.getDeleteResponseFilters()){
				filter.processDeleteResponseFilter(incommingRequest, outgoingRequest, incommingResponse, outgoingResponse);
			}
		}
	}
	
	private HttpPost generateBasicHttpPost(String path) {
		return new HttpPost(PATH + path);
	}
	
	private HttpGet generateBasicHttpGet(String path) {
		return new HttpGet(PATH + path);
	}
	
	private HttpDelete generateBasicHttpDelete(String path) {
		return new HttpDelete(PATH + path);
	}
	
	private HttpPut generateBasicHttpPut(String path) {
		return new HttpPut(PATH + path);
	}
	
	private static HttpClient getHttpClient() {
		return new DefaultHttpClient();
	}
	
	private String getRequestPath() throws InternalServerErrorException{
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(incommingRequest.getPathInfo());
		
		if(null != incommingRequest.getQueryString() && incommingRequest.getQueryString().length() > 0){
			stringBuilder.append("?");
			try {
				stringBuilder.append(UriUtils.encodeQuery(UriUtils.decode(incommingRequest.getQueryString(), "UTF-8"), "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				throw new InternalServerErrorException("Error while URI decoding original request query string");
			}
		}
		
		return stringBuilder.toString();
	}
}
