package br.com.abril.repeatplease.filter.comparator;

import java.util.Comparator;

import br.com.abril.repeatplease.filter.spec.response.GetResponseFilter;

public class GetResponseFilterComparator implements Comparator<GetResponseFilter> {

	@Override
	public int compare(GetResponseFilter o1, GetResponseFilter o2) {
		return o1.getGetResponseFilterOrder() - o2.getGetResponseFilterOrder();
	}

}
