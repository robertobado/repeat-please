package br.com.abril.repeatplease.filter;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.springframework.stereotype.Component;

import br.com.abril.repeatplease.filter.spec.request.DeleteRequestFilter;
import br.com.abril.repeatplease.filter.spec.request.GetRequestFilter;
import br.com.abril.repeatplease.filter.spec.request.PostRequestFilter;
import br.com.abril.repeatplease.filter.spec.request.PutRequestFilter;
import br.com.abril.repeatplease.filter.spec.response.DeleteResponseFilter;
import br.com.abril.repeatplease.filter.spec.response.GetResponseFilter;
import br.com.abril.repeatplease.filter.spec.response.PostResponseFilter;
import br.com.abril.repeatplease.filter.spec.response.PutResponseFilter;
import br.com.abril.repeatplease.util.SystemConfiguration;

@Component
public class HeaderPropagateFilter implements GetRequestFilter, PostRequestFilter, PutRequestFilter, DeleteRequestFilter, GetResponseFilter, PostResponseFilter, PutResponseFilter, DeleteResponseFilter{

	private static final Integer filterOrder = 0;
	
	private static final String REQUEST_HEADERS_PROPAGATE_LIST_CONFIG_KEY = "header.propagate.request.list";
	private static final String RESPONSE_HEADERS_PROPAGATE_LIST_CONFIG_KEY = "header.propagate.response.list";
	private static List<String> requestHeadersToPropagate;
	private static List<String> responseHeadersToPropagate;
	
	@Override
	public void processDeleteRequestFilter(HttpServletRequest incommingRequest, HttpUriRequest outgoingRequest)
			throws Throwable {
		processRequestFilter(incommingRequest, outgoingRequest);
	}

	@Override
	public void processPutRequestFilter(HttpServletRequest incommingRequest, HttpUriRequest outgoingRequest)
			throws Throwable {
		processRequestFilter(incommingRequest, outgoingRequest);
	}

	@Override
	public void processPostRequestFilter(HttpServletRequest incommingRequest, HttpUriRequest outgoingRequest)
			throws Throwable {
		processRequestFilter(incommingRequest, outgoingRequest);
	}

	@Override
	public void processGetRequestFilter(HttpServletRequest incommingRequest, HttpUriRequest outgoingRequest)
			throws Throwable {
		processRequestFilter(incommingRequest, outgoingRequest);
	}

	private void processRequestFilter(HttpServletRequest incommingRequest, HttpUriRequest outgoingRequest){
		List<String> myHeadersToPropagate = getRequestHeadersToPropagate();
		
		for(String headerName : myHeadersToPropagate){
			String headerValue = incommingRequest.getHeader(headerName);
			
			if(null != headerValue){
				outgoingRequest.addHeader(headerName, headerValue);
			}
		}
		
	}
	
	private synchronized List<String> getRequestHeadersToPropagate(){
		if (null == requestHeadersToPropagate) {
			requestHeadersToPropagate = new LinkedList<String>();
			String headersToPropagateAsFullString = SystemConfiguration.getPropertyAsString(REQUEST_HEADERS_PROPAGATE_LIST_CONFIG_KEY);
			String[] headersToPropagateArray = headersToPropagateAsFullString.split(",");

			for (String header : headersToPropagateArray) {
				requestHeadersToPropagate.add(header.trim());
			}
		}
		
		return requestHeadersToPropagate;
	}
	
	private synchronized List<String> getResponseHeadersToPropagate(){
		if (null == responseHeadersToPropagate) {
			responseHeadersToPropagate = new LinkedList<String>();
			String headersToPropagateAsFullString = SystemConfiguration.getPropertyAsString(RESPONSE_HEADERS_PROPAGATE_LIST_CONFIG_KEY);
			String[] headersToPropagateArray = headersToPropagateAsFullString.split(",");

			for (String header : headersToPropagateArray) {
				responseHeadersToPropagate.add(header.trim());
			}
		}
		
		return responseHeadersToPropagate;
	}

	@Override
	public void processDeleteResponseFilter(
			HttpServletRequest incommingRequest,
			HttpUriRequest outgoingRequest, HttpResponse incommingResponse,
			ResponseBuilder outgoingResponse) throws Throwable {
		processResponseFilter(incommingResponse, outgoingResponse);
	}

	@Override
	public void processPutResponseFilter(HttpServletRequest incommingRequest,
			HttpUriRequest outgoingRequest, HttpResponse incommingResponse,
			ResponseBuilder outgoingResponse) throws Throwable {
		processResponseFilter(incommingResponse, outgoingResponse);
	}

	@Override
	public void processPostResponseFilter(HttpServletRequest incommingRequest,
			HttpUriRequest outgoingRequest, HttpResponse incommingResponse,
			ResponseBuilder outgoingResponse) throws Throwable {
		processResponseFilter(incommingResponse, outgoingResponse);
	}

	@Override
	public void processGetResponseFilter(HttpServletRequest incommingRequest,
			HttpUriRequest outgoingRequest, HttpResponse incommingResponse,
			ResponseBuilder outgoingResponse) throws Throwable {
		processResponseFilter(incommingResponse, outgoingResponse);
	}
	
	private void processResponseFilter(HttpResponse incommingResponse,
			ResponseBuilder outgoingResponse) {
		List<String> myHeadersToPropagate = getResponseHeadersToPropagate();
		
		for(String headerName : myHeadersToPropagate){
			Header header = incommingResponse.getFirstHeader(headerName);
			
			if(null != header){
				outgoingResponse.header(header.getName(), header.getValue());
			}
		}
	}

	@Override
	public int getDeleteResponseFilterOrder() {
		return filterOrder;
	}

	@Override
	public int getPutResponseFilterOrder() {
		return filterOrder;
	}

	@Override
	public int getPostResponseFilterOrder() {
		return filterOrder;
	}

	@Override
	public int getGetResponseFilterOrder() {
		return filterOrder;
	}

	@Override
	public int getDeleteRequestFilterOrder() {
		return filterOrder;
	}

	@Override
	public int getPutRequestFilterOrder() {
		return filterOrder;
	}

	@Override
	public int getPostRequestFilterOrder() {
		return filterOrder;
	}

	@Override
	public int getGetRequestFilterOrder() {
		return filterOrder;
	}
	
}
