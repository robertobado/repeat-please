package br.com.abril.repeatplease.filter.comparator;

import java.util.Comparator;

import br.com.abril.repeatplease.filter.spec.response.DeleteResponseFilter;

public class DeleteResponseFilterComparator implements Comparator<DeleteResponseFilter> {

	@Override
	public int compare(DeleteResponseFilter o1, DeleteResponseFilter o2) {
		return o1.getDeleteResponseFilterOrder() - o2.getDeleteResponseFilterOrder();
	}

}
