package br.com.abril.repeatplease.filter.spec.response;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;

public interface GetResponseFilter {
	
	public void processGetResponseFilter(HttpServletRequest incommingRequest, HttpUriRequest outgoingRequest, HttpResponse incommingResponse, ResponseBuilder outgoingResponse) throws Throwable;

	public int getGetResponseFilterOrder();
}
