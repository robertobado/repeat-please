package br.com.abril.repeatplease.filter.comparator;

import java.util.Comparator;

import br.com.abril.repeatplease.filter.spec.response.PutResponseFilter;

public class PutResponseFilterComparator implements Comparator<PutResponseFilter> {

	@Override
	public int compare(PutResponseFilter o1, PutResponseFilter o2) {
		return o1.getPutResponseFilterOrder() - o2.getPutResponseFilterOrder();
	}

}
