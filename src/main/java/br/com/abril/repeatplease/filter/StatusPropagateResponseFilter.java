package br.com.abril.repeatplease.filter;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.springframework.stereotype.Component;

import br.com.abril.repeatplease.filter.spec.response.DeleteResponseFilter;
import br.com.abril.repeatplease.filter.spec.response.GetResponseFilter;
import br.com.abril.repeatplease.filter.spec.response.PostResponseFilter;
import br.com.abril.repeatplease.filter.spec.response.PutResponseFilter;

@Component
public class StatusPropagateResponseFilter implements GetResponseFilter, PostResponseFilter, PutResponseFilter, DeleteResponseFilter{
	
	private static final Integer filterOrder = 1;
	
	@Override
	public void processDeleteResponseFilter(
			HttpServletRequest incommingRequest,
			HttpUriRequest outgoingRequest, HttpResponse incommingResponse,
			ResponseBuilder outgoingResponse) throws Throwable {
		processResponse(incommingResponse, outgoingResponse);
	}

	@Override
	public void processPutResponseFilter(HttpServletRequest incommingRequest,
			HttpUriRequest outgoingRequest, HttpResponse incommingResponse,
			ResponseBuilder outgoingResponse) throws Throwable {
		processResponse(incommingResponse, outgoingResponse);
	}

	@Override
	public void processPostResponseFilter(HttpServletRequest incommingRequest,
			HttpUriRequest outgoingRequest, HttpResponse incommingResponse,
			ResponseBuilder outgoingResponse) throws Throwable {
		processResponse(incommingResponse, outgoingResponse);
	}

	@Override
	public void processGetResponseFilter(HttpServletRequest incommingRequest,
			HttpUriRequest outgoingRequest, HttpResponse incommingResponse,
			ResponseBuilder outgoingResponse) throws Throwable {
		processResponse(incommingResponse, outgoingResponse);
	}

	private void processResponse(HttpResponse incommingResponse,
			ResponseBuilder outgoingResponse) {
		outgoingResponse.status(incommingResponse.getStatusLine().getStatusCode());
	}

	@Override
	public int getDeleteResponseFilterOrder() {
		return filterOrder;
	}

	@Override
	public int getPutResponseFilterOrder() {
		return filterOrder;
	}

	@Override
	public int getPostResponseFilterOrder() {
		return filterOrder;
	}

	@Override
	public int getGetResponseFilterOrder() {
		return filterOrder;
	}

}
