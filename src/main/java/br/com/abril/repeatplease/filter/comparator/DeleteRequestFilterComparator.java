package br.com.abril.repeatplease.filter.comparator;

import java.util.Comparator;

import br.com.abril.repeatplease.filter.spec.request.DeleteRequestFilter;

public class DeleteRequestFilterComparator implements Comparator<DeleteRequestFilter> {

	@Override
	public int compare(DeleteRequestFilter o1, DeleteRequestFilter o2) {
		return o1.getDeleteRequestFilterOrder() - o2.getDeleteRequestFilterOrder();
	}

}
