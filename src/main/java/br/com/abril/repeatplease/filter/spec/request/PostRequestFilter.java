package br.com.abril.repeatplease.filter.spec.request;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.methods.HttpUriRequest;

public interface PostRequestFilter {
	
	public void processPostRequestFilter(HttpServletRequest incommingRequest, HttpUriRequest outgoingRequest) throws Throwable;

	public int getPostRequestFilterOrder();
}
