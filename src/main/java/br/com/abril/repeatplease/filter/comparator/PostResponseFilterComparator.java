package br.com.abril.repeatplease.filter.comparator;

import java.util.Comparator;

import br.com.abril.repeatplease.filter.spec.response.PostResponseFilter;

public class PostResponseFilterComparator implements Comparator<PostResponseFilter> {

	@Override
	public int compare(PostResponseFilter o1, PostResponseFilter o2) {
		return o1.getPostResponseFilterOrder() - o2.getPostResponseFilterOrder();
	}

}
