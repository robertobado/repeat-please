package br.com.abril.repeatplease.filter;

import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import br.com.abril.repeatplease.filter.spec.request.PostRequestFilter;
import br.com.abril.repeatplease.filter.spec.request.PutRequestFilter;
import br.com.abril.repeatplease.filter.spec.response.DeleteResponseFilter;
import br.com.abril.repeatplease.filter.spec.response.GetResponseFilter;
import br.com.abril.repeatplease.filter.spec.response.PostResponseFilter;
import br.com.abril.repeatplease.filter.spec.response.PutResponseFilter;

@Component
public class BodyPropagateFilter implements PostRequestFilter, PutRequestFilter, GetResponseFilter, PostResponseFilter, PutResponseFilter, DeleteResponseFilter{

	private static final Integer filterOrder = 0;
	
	@Override
	public void processPutRequestFilter(HttpServletRequest incommingRequest,
			HttpUriRequest outgoingRequest) throws Throwable {
		processRequestFilter(incommingRequest, outgoingRequest);
	}

	@Override
	public void processPostRequestFilter(HttpServletRequest incommingRequest,
			HttpUriRequest outgoingRequest) throws Throwable {
		processRequestFilter(incommingRequest, outgoingRequest);
	}
	
	private void processRequestFilter(HttpServletRequest incommingRequest,
			HttpUriRequest outgoingRequest) throws IOException{
		
		ServletInputStream inputStream = incommingRequest.getInputStream();
		StringWriter writer = new StringWriter();
		IOUtils.copy(inputStream, writer, "UTF-8");
		String requestBody = writer.toString();

		if (null != requestBody) {

			StringEntity entity = new StringEntity(requestBody);

			if (outgoingRequest instanceof HttpPost) {
				((HttpPost) outgoingRequest).setEntity(entity);

			} else if (outgoingRequest instanceof HttpPut) {
				((HttpPut) outgoingRequest).setEntity(entity);
			}
		}
	}

	@Override
	public void processDeleteResponseFilter(
			HttpServletRequest incommingRequest,
			HttpUriRequest outgoingRequest, HttpResponse incommingResponse,
			ResponseBuilder outgoingResponse) throws Throwable {
		processResponseFilter(incommingResponse, outgoingResponse);
		
	}

	@Override
	public void processPutResponseFilter(HttpServletRequest incommingRequest,
			HttpUriRequest outgoingRequest, HttpResponse incommingResponse,
			ResponseBuilder outgoingResponse) throws Throwable {
		processResponseFilter(incommingResponse, outgoingResponse);
	}

	@Override
	public void processPostResponseFilter(HttpServletRequest incommingRequest,
			HttpUriRequest outgoingRequest, HttpResponse incommingResponse,
			ResponseBuilder outgoingResponse) throws Throwable {
		processResponseFilter(incommingResponse, outgoingResponse);
	}

	@Override
	public void processGetResponseFilter(HttpServletRequest incommingRequest,
			HttpUriRequest outgoingRequest, HttpResponse incommingResponse,
			ResponseBuilder outgoingResponse) throws Throwable {
		processResponseFilter(incommingResponse, outgoingResponse);
	}
	
	private void processResponseFilter(HttpResponse incommingResponse,
			ResponseBuilder outgoingResponse) throws ParseException, IOException {
		if(null != incommingResponse.getEntity()){
			String httpResponseAsString = EntityUtils.toString(incommingResponse.getEntity(), "UTF-8");
			outgoingResponse.entity(httpResponseAsString);
		}
	}

	@Override
	public int getDeleteResponseFilterOrder() {
		return filterOrder;
	}

	@Override
	public int getPutResponseFilterOrder() {
		return filterOrder;
	}

	@Override
	public int getPostResponseFilterOrder() {
		return filterOrder;
	}

	@Override
	public int getGetResponseFilterOrder() {
		return filterOrder;
	}

	@Override
	public int getPutRequestFilterOrder() {
		return filterOrder;
	}

	@Override
	public int getPostRequestFilterOrder() {
		return filterOrder;
	}

}
