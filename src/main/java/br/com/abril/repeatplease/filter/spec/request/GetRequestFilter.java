package br.com.abril.repeatplease.filter.spec.request;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.methods.HttpUriRequest;

public interface GetRequestFilter{
	
	public void processGetRequestFilter(HttpServletRequest incommingRequest, HttpUriRequest outgoingRequest) throws Throwable;
	
	public int getGetRequestFilterOrder();

}
