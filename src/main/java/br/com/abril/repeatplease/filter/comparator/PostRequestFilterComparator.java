package br.com.abril.repeatplease.filter.comparator;

import java.util.Comparator;

import br.com.abril.repeatplease.filter.spec.request.PostRequestFilter;

public class PostRequestFilterComparator implements Comparator<PostRequestFilter> {

	@Override
	public int compare(PostRequestFilter o1, PostRequestFilter o2) {
		return o1.getPostRequestFilterOrder() - o2.getPostRequestFilterOrder();
	}

}
