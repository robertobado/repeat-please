package br.com.abril.repeatplease.filter.comparator;

import java.util.Comparator;

import br.com.abril.repeatplease.filter.spec.request.GetRequestFilter;

public class GetRequestFilterComparator implements Comparator<GetRequestFilter> {

	@Override
	public int compare(GetRequestFilter o1, GetRequestFilter o2) {
		return o1.getGetRequestFilterOrder() - o2.getGetRequestFilterOrder();
	}

}
