package br.com.abril.repeatplease.filter;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.params.HttpParams;

import br.com.abril.repeatplease.filter.spec.request.GetRequestFilter;

public class QueryParamPropagateFilter implements GetRequestFilter{

	private static final Integer filterOrder = 0;
	
	@Override
	public void processGetRequestFilter(HttpServletRequest incommingRequest,
			HttpUriRequest outgoingRequest) throws Throwable {
		Enumeration<?> parameterNames = incommingRequest.getParameterNames();
		
		HttpParams params = outgoingRequest.getParams();
		
		while(parameterNames.hasMoreElements()){
			String parameterName = parameterNames.nextElement().toString();
			String parameterValue = incommingRequest.getParameter(parameterName);
			params.setParameter(parameterName, parameterValue);
		}
		
	}

	@Override
	public int getGetRequestFilterOrder() {
		return filterOrder;
	}

}
