package br.com.abril.repeatplease.filter.container;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.abril.repeatplease.filter.comparator.DeleteRequestFilterComparator;
import br.com.abril.repeatplease.filter.comparator.DeleteResponseFilterComparator;
import br.com.abril.repeatplease.filter.comparator.GetRequestFilterComparator;
import br.com.abril.repeatplease.filter.comparator.GetResponseFilterComparator;
import br.com.abril.repeatplease.filter.comparator.PostRequestFilterComparator;
import br.com.abril.repeatplease.filter.comparator.PostResponseFilterComparator;
import br.com.abril.repeatplease.filter.comparator.PutRequestFilterComparator;
import br.com.abril.repeatplease.filter.comparator.PutResponseFilterComparator;
import br.com.abril.repeatplease.filter.spec.request.DeleteRequestFilter;
import br.com.abril.repeatplease.filter.spec.request.GetRequestFilter;
import br.com.abril.repeatplease.filter.spec.request.PostRequestFilter;
import br.com.abril.repeatplease.filter.spec.request.PutRequestFilter;
import br.com.abril.repeatplease.filter.spec.response.DeleteResponseFilter;
import br.com.abril.repeatplease.filter.spec.response.GetResponseFilter;
import br.com.abril.repeatplease.filter.spec.response.PostResponseFilter;
import br.com.abril.repeatplease.filter.spec.response.PutResponseFilter;

@Component
public class FilterContainer {
	
	private Boolean ordered = false;

	@Autowired
	private List<GetRequestFilter> getRequestFilters;
	
	@Autowired
	private List<PostRequestFilter> postRequestFilters;
	
	@Autowired
	private List<PutRequestFilter> putRequestFilters;
	
	@Autowired
	private List<DeleteRequestFilter> deleteRequestFilters;
	
	@Autowired
	private List<GetResponseFilter> getResponseFilters;
	
	@Autowired
	private List<PostResponseFilter> postResponseFilters;
	
	@Autowired
	private List<PutResponseFilter> putResponseFilters;
	
	@Autowired
	private List<DeleteResponseFilter> deleteResponseFilters;

	public List<GetRequestFilter> getGetRequestFilters() {
		checkOrdering();
		return getRequestFilters;
	}

	public List<PostRequestFilter> getPostRequestFilters() {
		checkOrdering();
		return postRequestFilters;
	}

	public List<PutRequestFilter> getPutRequestFilters() {
		checkOrdering();
		return putRequestFilters;
	}

	public List<DeleteRequestFilter> getDeleteRequestFilters() {
		checkOrdering();
		return deleteRequestFilters;
	}

	public List<GetResponseFilter> getGetResponseFilters() {
		checkOrdering();
		return getResponseFilters;
	}

	public List<PostResponseFilter> getPostResponseFilters() {
		checkOrdering();
		return postResponseFilters;
	}

	public List<PutResponseFilter> getPutResponseFilters() {
		checkOrdering();
		return putResponseFilters;
	}

	public List<DeleteResponseFilter> getDeleteResponseFilters() {
		checkOrdering();
		return deleteResponseFilters;
	}
	
	private void checkOrdering(){
		if(!ordered){
			
			Collections.sort(getRequestFilters, new GetRequestFilterComparator());
			Collections.sort(postRequestFilters, new PostRequestFilterComparator());
			Collections.sort(putRequestFilters, new PutRequestFilterComparator());
			Collections.sort(deleteRequestFilters, new DeleteRequestFilterComparator());

			Collections.sort(getResponseFilters, new GetResponseFilterComparator());
			Collections.sort(postResponseFilters, new PostResponseFilterComparator());
			Collections.sort(putResponseFilters, new PutResponseFilterComparator());
			Collections.sort(deleteResponseFilters, new DeleteResponseFilterComparator());
			
			ordered = true;
		}
	}
}
