package br.com.abril.repeatplease.filter.comparator;

import java.util.Comparator;

import br.com.abril.repeatplease.filter.spec.request.PutRequestFilter;

public class PutRequestFilterComparator implements Comparator<PutRequestFilter> {

	@Override
	public int compare(PutRequestFilter o1, PutRequestFilter o2) {
		return o1.getPutRequestFilterOrder() - o2.getPutRequestFilterOrder();
	}

}
